require 'test_helper'

class ChromasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @chroma = chromas(:one)
  end

  test "should get index" do
    get chromas_url
    assert_response :success
  end

  test "should get new" do
    get new_chroma_url
    assert_response :success
  end

  test "should create chroma" do
    assert_difference('Chroma.count') do
      post chromas_url, params: { chroma: { description: @chroma.description, modification: @chroma.modification, name: @chroma.name, price: @chroma.price } }
    end

    assert_redirected_to chroma_url(Chroma.last)
  end

  test "should show chroma" do
    get chroma_url(@chroma)
    assert_response :success
  end

  test "should get edit" do
    get edit_chroma_url(@chroma)
    assert_response :success
  end

  test "should update chroma" do
    patch chroma_url(@chroma), params: { chroma: { description: @chroma.description, modification: @chroma.modification, name: @chroma.name, price: @chroma.price } }
    assert_redirected_to chroma_url(@chroma)
  end

  test "should destroy chroma" do
    assert_difference('Chroma.count', -1) do
      delete chroma_url(@chroma)
    end

    assert_redirected_to chromas_url
  end
end
