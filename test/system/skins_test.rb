require "application_system_test_case"

class SkinsTest < ApplicationSystemTestCase
  setup do
    @skin = skins(:one)
  end

  test "visiting the index" do
    visit skins_url
    assert_selector "h1", text: "Skins"
  end

  test "creating a Skin" do
    visit skins_url
    click_on "New Skin"

    fill_in "Chroma", with: @skin.chroma_id
    fill_in "Description", with: @skin.description
    fill_in "Modification", with: @skin.modification
    fill_in "Name", with: @skin.name
    fill_in "Price", with: @skin.price
    click_on "Create Skin"

    assert_text "Skin was successfully created"
    click_on "Back"
  end

  test "updating a Skin" do
    visit skins_url
    click_on "Edit", match: :first

    fill_in "Chroma", with: @skin.chroma_id
    fill_in "Description", with: @skin.description
    fill_in "Modification", with: @skin.modification
    fill_in "Name", with: @skin.name
    fill_in "Price", with: @skin.price
    click_on "Update Skin"

    assert_text "Skin was successfully updated"
    click_on "Back"
  end

  test "destroying a Skin" do
    visit skins_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Skin was successfully destroyed"
  end
end
