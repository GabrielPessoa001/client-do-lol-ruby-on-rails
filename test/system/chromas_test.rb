require "application_system_test_case"

class ChromasTest < ApplicationSystemTestCase
  setup do
    @chroma = chromas(:one)
  end

  test "visiting the index" do
    visit chromas_url
    assert_selector "h1", text: "Chromas"
  end

  test "creating a Chroma" do
    visit chromas_url
    click_on "New Chroma"

    fill_in "Description", with: @chroma.description
    fill_in "Modification", with: @chroma.modification
    fill_in "Name", with: @chroma.name
    fill_in "Price", with: @chroma.price
    click_on "Create Chroma"

    assert_text "Chroma was successfully created"
    click_on "Back"
  end

  test "updating a Chroma" do
    visit chromas_url
    click_on "Edit", match: :first

    fill_in "Description", with: @chroma.description
    fill_in "Modification", with: @chroma.modification
    fill_in "Name", with: @chroma.name
    fill_in "Price", with: @chroma.price
    click_on "Update Chroma"

    assert_text "Chroma was successfully updated"
    click_on "Back"
  end

  test "destroying a Chroma" do
    visit chromas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Chroma was successfully destroyed"
  end
end
