Rails.application.routes.draw do
  resources :notices
  devise_for :users
  resources :users
  resources :stores
  resources :packages
  resources :champions
  resources :skins
  resources :chromas

  get '/dashboard', to: 'pages#dashboard'
  get '/perfil', to: 'pages#perfil'
  get '/colecao', to: 'pages#colecao'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end