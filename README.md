Projeto criado em parceria entre Átila Santos e Gabriel Pessoa, dois programadores iniciantes que buscam treinar suas capacidades tanto com front quanto com back end utilizando as tecnologias expressas a seguir:

-> RUBY ON RAILS como framework para o back e MySQL como banco de dados configurado no arquivo config/database.yml presente nos diretórios criados com rails new my-app.

-> CONFIGURAÇÕES:

---- Ruby: versão 2.5.5 (Gemfile com todas as gemas utilizadas em nossa aplicação);
---- Rails: versão 5.2.5;
---- javascript puro, JQuery e Node;
---- HTML e CSS com Semantic para ajudar na questão gráfica em alguns quesitos;

-> COMO RODAR A APLICAÇÃO:

---- Após o git pull origin master, você vai configurar em seu rvm (gerenciador de versões Ruby) as versões especificadas anteriormente do Ruby e Rails -> [ rvm use 2.5.5 ];
----- Após a configuração do ambiente, é preciso configurar suas gemas através do arquivo Gemfile e, para isso, entre no projeto e use o comando bundle install que possui todos os locais e direções para as gemas utilizadas em nosso projeto;
----- Também é preciso configurar o ambiente Node com os pacotes específicos da lib através do NPM. Rode o comando npm install para instalar todas essas dependências que estão no arquivo package.json;
---- Configure o seu arquivo database.yml de acordo com os bancos de dados que você pretende utilizar em seu projeto, mas vale ressaltar que esses bancos de dados precisam existir dentro do seu MySQL para tudo rodar de maneira adequada;
---- Após tudo configurado, rode o comando rails s para rodar a nossa aplicação e conferir um pouco mais do projeto.