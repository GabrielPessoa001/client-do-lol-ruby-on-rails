class Store < ApplicationRecord
  belongs_to :package
  belongs_to :champion
  belongs_to :skin
  belongs_to :chroma
end
