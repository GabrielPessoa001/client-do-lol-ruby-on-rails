class Skin < ApplicationRecord
  belongs_to :chroma

  validates :name, presence: true
  validates :price, presence: true
  validates :description, presence: true
  validates :modification, presence: true

  validates :chroma, presence: false
end
