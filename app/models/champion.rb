class Champion < ApplicationRecord
  belongs_to :skin

  validates :name, presence: true
  validates :price, presence: true
  validates :description, presence: true

  validates :skin, presence: false
end
