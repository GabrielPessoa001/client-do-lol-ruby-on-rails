class Package < ApplicationRecord
  belongs_to :champion
  belongs_to :skin
  belongs_to :chroma
end
