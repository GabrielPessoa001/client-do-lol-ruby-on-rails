class ChromasController < ApplicationController
  before_action :set_chroma, only: [:show, :edit, :update, :destroy]

  # GET /chromas
  # GET /chromas.json
  def index
    @chromas = Chroma.all
  end

  # GET /chromas/1
  # GET /chromas/1.json
  def show
  end

  # GET /chromas/new
  def new
    @chroma = Chroma.new
  end

  # GET /chromas/1/edit
  def edit
  end

  # POST /chromas
  # POST /chromas.json
  def create
    @chroma = Chroma.new(chroma_params)

    respond_to do |format|
      if @chroma.save
        format.html { redirect_to @chroma, notice: 'Chroma was successfully created.' }
        format.json { render :show, status: :created, location: @chroma }
      else
        format.html { render :new }
        format.json { render json: @chroma.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /chromas/1
  # PATCH/PUT /chromas/1.json
  def update
    respond_to do |format|
      if @chroma.update(chroma_params)
        format.html { redirect_to @chroma, notice: 'Chroma was successfully updated.' }
        format.json { render :show, status: :ok, location: @chroma }
      else
        format.html { render :edit }
        format.json { render json: @chroma.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chromas/1
  # DELETE /chromas/1.json
  def destroy
    @chroma.destroy
    respond_to do |format|
      format.html { redirect_to chromas_url, notice: 'Chroma was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_chroma
      @chroma = Chroma.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def chroma_params
      params.require(:chroma).permit(:name, :price, :description, :modification)
    end
end
