class PagesController < ApplicationController

  layout "pages"

  @notices = Notice.all;

  def dashboard
    render "pages/dashboard"
  end

  def perfil
    render "pages/perfil"
  end

  def colecao
    render "pages/colecao"
  end
end
