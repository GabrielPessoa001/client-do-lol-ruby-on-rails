json.extract! chroma, :id, :name, :price, :description, :modification, :created_at, :updated_at
json.url chroma_url(chroma, format: :json)
