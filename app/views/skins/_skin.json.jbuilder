json.extract! skin, :id, :name, :price, :description, :modification, :chroma_id, :created_at, :updated_at
json.url skin_url(skin, format: :json)
