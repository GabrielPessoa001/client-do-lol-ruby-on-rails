json.extract! store, :id, :package_id, :champion_id, :skin_id, :chroma_id, :created_at, :updated_at
json.url store_url(store, format: :json)
