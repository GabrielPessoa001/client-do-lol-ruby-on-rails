json.extract! package, :id, :name, :price, :champion_id, :skin_id, :chroma_id, :created_at, :updated_at
json.url package_url(package, format: :json)
