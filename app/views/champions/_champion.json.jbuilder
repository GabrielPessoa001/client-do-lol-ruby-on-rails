json.extract! champion, :id, :name, :price, :description, :skin_id, :created_at, :updated_at
json.url champion_url(champion, format: :json)
