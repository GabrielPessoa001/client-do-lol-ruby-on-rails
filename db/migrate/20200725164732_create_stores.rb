class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.references :package, foreign_key: true
      t.references :champion, foreign_key: true
      t.references :skin, foreign_key: true
      t.references :chroma, foreign_key: true

      t.timestamps
    end
  end
end
