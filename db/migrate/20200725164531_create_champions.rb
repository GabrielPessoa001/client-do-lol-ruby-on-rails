class CreateChampions < ActiveRecord::Migration[5.2]
  def change
    create_table :champions do |t|
      t.string :name
      t.integer :price
      t.text :description
      t.references :skin, foreign_key: true

      t.timestamps
    end
  end
end
