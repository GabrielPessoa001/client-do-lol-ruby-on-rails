class CreatePackages < ActiveRecord::Migration[5.2]
  def change
    create_table :packages do |t|
      t.string :name
      t.integer :price
      t.references :champion, foreign_key: true
      t.references :skin, foreign_key: true
      t.references :chroma, foreign_key: true

      t.timestamps
    end
  end
end
