class CreateChromas < ActiveRecord::Migration[5.2]
  def change
    create_table :chromas do |t|
      t.string :name
      t.integer :price
      t.string :description
      t.string :modification

      t.timestamps
    end
  end
end
